package main

import (
	"fmt"
	"gitlab.com/sshaharse1/goportrange"
)

func main() {
	ports, err := Goportrange.PortsToSlice("-5,50,200,65500-")
	if err != nil {
		panic(err)
	}
	fmt.Printf("%+v", ports)
}


