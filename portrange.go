package GOportrange

import (
	"errors"
	"strconv"
	"strings"
)

const (
	expressionSeperator = ","
	rangeSeperator = "-"
)

// PortsToSlice converts a string port range into a slice
// "1,2,3-6" -> [1,2,3,4,5,6]
// "-5,9,10" -> [1,2,3,4,5,9,10]
// "1,2,3,1,2,3," -> [1,2,3]
func PortsToSlice(ports string) ([]int, error) {
	var portSlice []int
	expressions := strings.Split(ports, expressionSeperator)
	for _, expr := range expressions {
		exprPortRange, err := expressionToSlice(expr)
		if err != nil {
			return portSlice, err
		}
		portSlice = appendToSlice(portSlice, exprPortRange)
	}
	return portSlice, nil
}

func appendToSlice(firstSlice []int, secondSlice []int) []int {
	for _, element := range secondSlice {
		if !inSlice(firstSlice, element) {
			firstSlice = append(firstSlice, element)
		}
	}
	return firstSlice
}

func inSlice(slice []int, val int) bool {
	for _, element := range slice {
		if element == val {
			return true
		}
	}
	return false
}

func expressionToSlice(ports string) ([]int, error) {
	if strings.Contains(ports, rangeSeperator) {
		port, err := rangeToSlice(ports)
		return port, err
	}
	port, err := strconv.Atoi(ports)
	return []int{port}, err
}

func rangeToSlice(portRange string) ([]int, error) {
	var ports []int
	min, max, err := rangeToMinMax(portRange)
	if err != nil {
		return ports, err
	}
	for i := min; i <= max; i++ {
		ports = append(ports, i)
	}
	return ports, nil
}

func rangeToMinMax(portRange string) (int, int, error) {
	min := 0
	max := 0
	var err error

	ports := strings.Split(portRange, rangeSeperator)

	if len(ports) > 2 {
		return 0, 0, errors.New("too many brackets in expression")
	}

	if len(ports[0]) == 0 {
		min = 1
	} else {
		min, err = strconv.Atoi(ports[0])
		if err != nil {
			return 0, 0, err
		}
	}

	if len(ports[1]) == 0 {
		max = 65535
	} else {
		max, err = strconv.Atoi(ports[1])
		if err != nil {
			return 0, 0, err
		}
	}
	if min > max {
		return max, min, nil
	}
	return min, max, nil
}
