# GOportrange

Lets you easily convert port ranges to slices

## Features

* Can handle ranges without a given ending (5- will give ports from 5 till max port num, -5 will give ports 1-5)
* Removes duplicates

## Getting Started

```
import "gitlab.com/sshaharse1/goportrange"
```

Converting a string into a slice
```
GOportrange.PortsToSlice(string)
```

## Examples

#### Example 1
```
GOportrange.PortsToSlice("-5,50,200") 
```
This will return a slice with
```
[1,2,3,4,5,50,200]
```
#### Example 2
```
GOportrange.PortsToSlice("1,2,3,1,2,3")
```
This will return a slice with
```
[1,2,3]
```
#### Example 3
```
GOportrange.PortstoSlice("-")
```
This will return a slice with all valid ports

