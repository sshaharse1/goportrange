package GOportrange

import (
	"reflect"
	"testing"
)

func TestPortsToSlice(t *testing.T) {
	ports, err := PortsToSlice("-5")
	if err != nil {
		t.Errorf("Error occured %+v", err)
	}
	if !reflect.DeepEqual(ports, []int{1,2,3,4,5}) {
		t.Errorf("1-5 test has failed!")
	}

	ports, err = PortsToSlice("-")
	if err != nil {
		t.Errorf("Error occured %+v", err)
	}
	if len(ports) != 65535 {
		t.Errorf("All ports test failed")
	}

	ports, err = PortsToSlice("1,2,3,1,2,3")
	if err != nil {
		t.Errorf("Error occured %+v", err)
	}
	if !reflect.DeepEqual(ports, []int{1,2,3}) {
		t.Errorf("Duplicated test failed")
	}
}